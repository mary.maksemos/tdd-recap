
[![Coverage Report](https://gitlab.com/mary.maksemos/TDD-ReCap/badges/main/coverage.svg)](https://gitlab.com/mary.maksemos/TDD-ReCap/-/commits/main)

Check the detailed [coverage report xml format](https://mary.maksemos.gitlab.io/-/TDD-ReCap/-/jobs/5254068289/artifacts/coverage.xml) 


 Check the detailed [coverage report html format](https://mary.maksemos.gitlab.io/-/tdd-recap/-/jobs/5254068289/artifacts/coverage_html/index.html)

