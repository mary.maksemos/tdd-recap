
def read_depths_from_file(filename):
    """
    Read depth measurements from a file.
    """
    with open(filename, 'r') as file:
        return [int(line.strip()) for line in file]


def count_increases(depths):
    """
    Count how many times the depth increases when comparing to the previous reading.

    """
 
    return sum(1 for prev_depth, curr_depth in zip(depths, depths[1:]) if curr_depth > prev_depth)


if __name__ == "__main__":
    depths = read_depths_from_file('report.txt')
    result = count_increases(depths)
    print(result)
