def count_increases(measurements):
    """Count how many times a measurement increases from the previous one."""
    return sum(1 for i in range(1, len(measurements)) if measurements[i] > measurements[i-1])

def count_sliding_window_increases(measurements, window_size=3):
    """Count increases in the sum of a sliding window of measurements."""
    previous_sum = sum(measurements[:window_size])
    count = 0

    for i in range(1, len(measurements) - window_size + 1):
        current_sum = sum(measurements[i:i+window_size])
        if current_sum > previous_sum:
            count += 1
        previous_sum = current_sum

    return count

def main():
    # Read measurements from file
    with open("report.txt", "r") as file:
        measurements = [int(line.strip()) for line in file.readlines()]

    # Compute and display results
    increases_count = count_increases(measurements)
    larger_sums_count = count_sliding_window_increases(measurements)

    
    print(f"Number of sums in sliding window that increased: {larger_sums_count}")

if __name__ == "__main__":
    main()
