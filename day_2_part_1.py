def calculate_position_and_depth(course):
    horizontal_position = 0
    depth = 0

    for command in course:
        action, value = command.split() 
        value = int(value)

        if action == "forward":
            horizontal_position += value
        elif action == "down":
            depth += value
        elif action == "up":
            depth -= value
        else:
            raise ValueError(f"Unknown action: {action}")

    return horizontal_position, depth, horizontal_position * depth

def read_course_from_file(filename):
    with open(filename, 'r') as f:
        return [line.strip() for line in f.readlines()]

def main():
    filename = "course.txt"  
    course = read_course_from_file(filename)

    hp, dp, multiplication = calculate_position_and_depth(course)

    print(f"Horizontal Position: {hp}")
    print(f"Depth: {dp}")
    print(f"Multiplication: {multiplication}")

if __name__ == "__main__":
    main()
