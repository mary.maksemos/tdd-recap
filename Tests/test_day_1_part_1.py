import pytest
from day_1_part_1 import read_depths_from_file, count_increases

def test_read_depths_from_file():
    # Given a mock file with depths
    with open('test_report.txt', 'w') as file:
        file.write("10\n20\n30\n20\n")

    # When reading depths from the file
    depths = read_depths_from_file('test_report.txt')

    # Then the depths should match the expected list
    assert depths == [10, 20, 30, 20]


def test_count_increases():
    depths = [10, 20, 30, 20, 40, 50, 40]
    result = count_increases(depths)
    assert result == 4  # 10->20, 20->30, 30->40, 40->50


def teardown_module():
    # Cleanup the mock file after tests are run
    import os
    os.remove('test_report.txt')
