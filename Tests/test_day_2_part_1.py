import pytest
from day_2_part_1 import calculate_position_and_depth, read_course_from_file  

def test_calculate_position_and_depth():
    
    course = ["forward 5", "down 3", "forward 10", "up 2"]
    hp, dp, multiplication = calculate_position_and_depth(course)
    
    assert hp == 15
    assert dp == 1
    assert multiplication == 15


    course = ["forward 5", "jump 10"]
    with pytest.raises(ValueError):
        calculate_position_and_depth(course)

def test_read_course_from_file(tmpdir):
 
    data = "forward 5\ndown 3\nforward 10\nup 2\n"
    course_file = tmpdir.join("course.txt")
    course_file.write(data)
    
    course = read_course_from_file(course_file.strpath)
    
    assert course == ["forward 5", "down 3", "forward 10", "up 2"]

