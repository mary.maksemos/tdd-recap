import pytest
from day_2_part_2 import calculate_position_and_depth_p2, read_course_from_file

def test_calculate_position_and_depth_p2():
   
    course = [
        "forward 2",
        "down 1",
        "forward 3",
        "up 2",
        "forward 4"
    ]
    hp, dp, multiplication = calculate_position_and_depth_p2(course)
    
    assert hp == 9  
    assert dp == -1  
    assert multiplication == -9  



