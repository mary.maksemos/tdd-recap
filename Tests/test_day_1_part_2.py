import pytest
from day_1_part_2 import count_increases, count_sliding_window_increases

def test_count_increases():
    measurements = [10, 20, 10, 30, 20, 40, 30, 50]
    assert count_increases(measurements) == 4

def test_count_sliding_window_increases():
    measurements = [10, 20, 10, 30, 20, 40, 30, 50]
    
    # For the above measurements and window_size=3:
    # Windows: [10,20,10], [20,10,30], [10,30,20], [30,20,40], [20,40,30], [40,30,50]
    # Sums:    40, 60, 60, 90, 90, 120
    # Sums that increased from the previous: 4 (40->60, 60->90, 90->120)
    
    assert count_sliding_window_increases(measurements) == 3

